<?php

namespace Drupal\erp_transaction;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Datetime\DateFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the list builder for transactions.
 *
 * TODO The feature request of add payments related to transactions is marked here as #TRANSPAY
 * This new feature can be ported from: Drupal\commerce_payment\PaymentListBuilder
 */
class OrderTransactionsListBuilder extends EntityListBuilder
{
    /**
     * The current route match.
     *
     * @var \Drupal\Core\Routing\RouteMatchInterface
     */
    protected $routeMatch;

    /**
     * {@inheritdoc}
     */
    protected $entitiesKey = 'transactions';

    /**
     * Constructs a new OrderTransactionListBuilder object.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface                 $entity_type
     *   The entity type definition.
     * @param \Drupal\Core\Entity\EntityStorageInterface              $storage
     *   The entity storage class.
     * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
     *   The currency formatter.
     * @param \Drupal\Core\Routing\RouteMatchInterface                $route_match
     *   The current route match.
     * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
     *   The date formatter.
     */
    public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RouteMatchInterface $route_match, DateFormatter $date_formatter)
    {
        parent::__construct($entity_type, $storage);

        $this->routeMatch = $route_match;
        $this->dateFormatter = $date_formatter;
    }

    /**
     * {@inheritdoc}
     */
    public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type)
    {
        return new static(
            $entity_type,
            $container->get('entity.manager')->getStorage($entity_type->id()),
            $container->get('current_route_match'),
            $container->get('date.formatter')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'erp_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function load()
    {
        $order = $this->routeMatch->getParameter('commerce_order');
        if ($order) {
            return $this->storage->loadMultipleByOrder($order);
        }
        $product =  $this->routeMatch->getParameter('commerce_product');
        if ($product) {
            return $this->storage->loadMultipleByProduct($product);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultOperations(EntityInterface $entity)
    {
        return parent::getDefaultOperations($entity);
        $operations = [];// $this->buildOperations($entity);
        /**
   * @var \Drupal\commerce_payment\Entity\PaymentInterface $entity 
*/
        /* #TRANSPAY
        if ($entity->getPayment() ){
        // #TRANSPAY add payments related to transactions... (Feature request)
        if ($entity->getPaymentGateway() ){
        $payment_gateway_plugin = $entity->getPaymentGateway()->getPlugin();
        $operations = $payment_gateway_plugin->buildPaymentOperations($entity);
        }
        }*/
        // Filter out operations that aren't allowed.
        $operations = array_filter(
            $operations, function ($operation) {
                return !empty($operation['access']);
            }
        );

        // Build the url for each operation.
        $base_route_parameters = [
        'erp_transaction' => $entity->id(),
        //'commerce_order' => $entity->getOrderId(),
        ];
        foreach ($operations as $operation_id => $operation) {
            $route_parameters = $base_route_parameters; 
            //if ($entity->getPaymentGateway() : $route_parameters  = $route_parameters + ['operation' => $operation_id];
            $operation['url'] = new Url('entity.erp_transaction.edit_form', $route_parameters);
            $operations[$operation_id] = $operation;
        }


        // Add the non-gateway-specific operations.
        if ($entity->access('delete')) {
            $operations['delete'] = [
            'title' => $this->t('Delete'),
            'weight' => 100,
            'url' => $entity->toUrl('delete-form'),
            ];
        }
        return $operations;
    }

    /**
     * {@inheritdoc}
     */
    public function buildHeader()
    {
        $header['label'] = $this->t('Transaction');
        $header['resource'] = $this->t('Resource');
        $header['quantity'] = $this->t('Stock relative quantity');
        $header['description'] = $this->t('Description');
        $header['date'] = $this->t('Date');
        //$header['amount'] = $this->t('Payment')// #TRANSPAY
        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity)
    {
        $formatted_amount = 0.0;
        // #TRANSPAY
        //if ($entity->getPayment() ){
        /**
 * @var \Drupal\commerce_payment\Entity\PaymentInterface $entity 
*/
        //$formatted_amount = $entity->getPayment();
        //}
        // $row['amount'] = $formatted_amount; #TRANSPAY
        $row['label'] = $entity->getTitle();
        $resource_items = [];
        $resource_qties = [];
        foreach ($entity->getResource()->referencedEntities() as $resource_item){
          $resource_items[] = new FormattableMarkup("@label (@id)", ['@label' => $resource_item->label(), "@id" => $resource_item->id()]);
          $resource_qties[] = new FormattableMarkup("@substract@qty", ['@substract' => ($resource_item->get('substract')->value)?"-":"+", "@qty" => $resource_item->getQuantity()]);
        }
        $row['resource'] = new FormattableMarkup( implode("<br/>", $resource_items),[]);
        $row['quantity'] = new FormattableMarkup( "<pre>@qties</pre>",[ "@qties" => implode("\n", $resource_qties)]);
        $row['description'] = $entity->getDescription();
        $row['date'] = $this->dateFormatter->format($entity->getCreatedTime());

        return $row + parent::buildRow($entity);
    }

}
