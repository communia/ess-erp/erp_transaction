<?php

namespace Drupal\erp_transaction;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Transaction entities.
 *
 * @ingroup erp_transaction
 */
class TransactionListBuilder extends EntityListBuilder
{


    /**
     * {@inheritdoc}
     */
    public function buildHeader()
    {
        $header['id'] = $this->t('Transaction ID');
        $header['title'] = $this->t('Title');
        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity)
    {
        /* @var $entity \Drupal\erp_transaction\Entity\Transaction */
        $row['id'] = $entity->id();
        $row['title'] = Link::createFromRoute(
            $entity->label(),
            'entity.erp_transaction.edit_form',
            ['erp_transaction' => $entity->id()]
        );
        return $row + parent::buildRow($entity);
    }

}
