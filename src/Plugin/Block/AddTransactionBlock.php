<?php

namespace Drupal\erp_transaction\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webprofiler\Entity\EntityManagerWrapper;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\erp_transaction\Entity\Transaction;
/**
 * Provides a 'AddTransactionBlock' block.
 *
 * @Block(
 *  id = "add_transaction_block",
 *  admin_label = @Translation("Add transaction block"),
 *  context = {
 *    "order" = @ContextDefinition(
 *      "entity:commerce_order",
 *      label = @Translation("Current Order"),
 *      required = FALSE,
 *    )
 *  }
 * )
 */
class AddTransactionBlock extends BlockBase implements ContainerFactoryPluginInterface
{

    /**
     * Drupal\webprofiler\Entity\EntityManagerWrapper definition.
     *
     * @var \Drupal\webprofiler\Entity\EntityManagerWrapper
     */
    protected $entityTypeManager;
    /**
     * Drupal\Core\Entity\EntityFormBuilderInterface definition.
     *
     * @var \Drupal\Core\Entity\EntityFormBuilderInterface
     */
    protected $entityFormBuilder;
    /**
     * Drupal\Core\Entity\EntityManagerInterface definition.
     *
     * @var \Drupal\Core\Entity\EntityManagerInterface
     */
    protected $entityManager;
    /**
     * Constructs a new AddTransactionBlock object.
     *
     * @param array  $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param string $plugin_definition
     *   The plugin implementation definition.
     */
    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityManagerWrapper $entity_type_manager, 
        EntityFormBuilderInterface $entity_form_builder, 
        EntityManagerInterface $entity_manager
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->entityTypeManager = $entity_type_manager;
        $this->entityFormBuilder = $entity_form_builder;
        $this->entityManager = $entity_manager;
    }
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('entity_type.manager'),
            $container->get('entity.form_builder'),
            $container->get('entity.manager')
        );
    }
    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [
          ] + parent::defaultConfiguration();
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form['intext'] = [
        '#type' => 'textfield',
        '#title' => $this->t('intext'),
        '#default_value' => $this->configuration['intext'],
        '#maxlength' => 64,
        '#size' => 64,
        '#weight' => '0',
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        $this->configuration['intext'] = $form_state->getValue('intext');
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        // $build = [];
        // $build['add_transaction_block_intext']['#markup'] = '<p>' . $this->configuration['intext'] . '</p>';
        // return $build;
        $entity = Transaction::create(
            [
            'type' => 'delivery_note',
            ]
        );
        // TODO Add form mode "add transaction block"
        $transaction_form = $this->entityFormBuilder->getForm($entity, 'default');

        return $transaction_form;

    }
    /**
     * {@inheritdoc}
     */
    /*protected function blockAccess(AccountInterface $account) {
    // TODO SHOW OLY WHEN IN ORDER :
    //dpm($this->getContextValue('order'));
    //dpm($this->getContexts());
    if($this->getContextValue('order') == null){
          return AccessResult::forbidden();
    };
    //return AccessResult::allowedIfHasPermission($account, 'access content');
    }*/
}
