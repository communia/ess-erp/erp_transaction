<?php

namespace Drupal\erp_transaction;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\erp_transaction\Entity\TransactionInterface;

/**
 * Defines the storage handler class for Transaction entities.
 *
 * This extends the base storage class, adding required special handling for
 * Transaction entities. TODO add helper methods
 *
 * @ingroup erp_transaction
 */
interface TransactionStorageInterface extends ContentEntityStorageInterface
{

    /**
     * Gets a list of Transaction revision IDs for a specific Transaction.
     *
     * @param \Drupal\erp_transaction\Entity\TransactionInterface $entity
     *   The Transaction entity.
     *
     * @return int[]
     *   Transaction revision IDs (in ascending order).
    public function revisionIds(TransactionInterface $entity);
     */

    /**
     * Gets a list of revision IDs having a given user as Transaction author.
     *
     * @param \Drupal\Core\Session\AccountInterface $account
     *   The user entity.
     *
     * @return int[]
     *   Transaction revision IDs (in ascending order).
    public function userRevisionIds(AccountInterface $account);
     */

    /**
     * Counts the number of revisions in the default language.
     *
     * @param \Drupal\erp_transaction\Entity\TransactionInterface $entity
     *   The Transaction entity.
     *
     * @return int
     *   The number of revisions in the default language.
    public function countDefaultLanguageRevisions(TransactionInterface $entity);
     */

    /**
     * Unsets the language for all Transaction with the given language.
     *
     * @param \Drupal\Core\Language\LanguageInterface $language
     *   The language object.
    public function clearRevisionsLanguage(LanguageInterface $language);
     */

}
