<?php

namespace Drupal\erp_transaction\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Transaction entities.
 *
 * @ingroup erp_transaction
 */
class TransactionDeleteForm extends ContentEntityDeleteForm
{


}
