<?php

namespace Drupal\erp_transaction\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Transaction edit forms.
 *
 * @ingroup erp_transaction
 */
class TransactionForm extends ContentEntityForm
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        /* @var $entity \Drupal\erp_transaction\Entity\Transaction */
        $form = parent::buildForm($form, $form_state);

        $entity = $this->entity;

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        $entity = $this->entity;

        $status = parent::save($form, $form_state);

        switch ($status) {
        case SAVED_NEW:
            drupal_set_message(
                $this->t(
                    'Created the %label Transaction.', [
                    '%label' => $entity->label(),
                    ]
                )
            );
            break;

        default:
            drupal_set_message(
                $this->t(
                    'Saved the %label Transaction.', [
                    '%label' => $entity->label(),
                    ]
                )
            );
        }
        $form_state->setRedirect('entity.erp_transaction.canonical', ['erp_transaction' => $entity->id()]);
    }

}
