<?php

namespace Drupal\erp_transaction\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class RelatedTransactionsController.
 */
class RelatedTransactionsController extends ControllerBase
{
    /**
     * Listing.
     *
     * @return string
     *   Return Hello string.
     */
    public function listingByOrder($commerce_order)
    {
        $list = $this->entityTypeManager()->getHandler('erp_transaction', 'list_builder_by_order');
        return $list->render();
    }
    /**
     * Listingp.
     *
     * @return string
     *   Return Hello string.
     */
    public function listingByProduct($commerce_product)
    {
        $list = $this->entityTypeManager()->getHandler('erp_transaction', 'list_builder_by_product');
        return $list->render();
    }

}
