<?php

namespace Drupal\erp_transaction\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use \Drupal\commerce_order\Entity\OrderItemInterface;

/**
 * Defines the Transaction entity.
 *
 *     "list_builder" = "Drupal\erp_transaction\TransactionListBuilder",
 *     "list_builder" = "Drupal\erp_transaction\OrderTransactionListBuilder"
 *
 *
 *     "collection" = "/admin/structure/erp_transaction"
 *     "collection-order" = "/admin/commerce/orders/{commerce_order}/transactions",
 *     "collection-resource" = "/product/{commerce_product}/transactions"

 * @ingroup erp_transaction
 *
 * @ContentEntityType(
 *   id = "erp_transaction",
 *   label = @Translation("Transaction"),
 *   label_collection = @Translation("Transactions"),
 *   bundle_label = @Translation("Transaction type"),
 *   handlers = {
 *     "storage" = "Drupal\erp_transaction\TransactionStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\erp_transaction\TransactionListBuilder",
 *     "list_builder_by_order" = "Drupal\erp_transaction\OrderTransactionsListBuilder",
 *     "list_builder_by_product" = "Drupal\erp_transaction\OrderTransactionsListBuilder",
 *     "views_data" = "Drupal\erp_transaction\Entity\TransactionViewsData",
 *     "translation" = "Drupal\erp_transaction\TransactionTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\erp_transaction\Form\TransactionForm",
 *       "add" = "Drupal\erp_transaction\Form\TransactionForm",
 *       "edit" = "Drupal\erp_transaction\Form\TransactionForm",
 *       "delete" = "Drupal\erp_transaction\Form\TransactionDeleteForm",
 *     },
 *     "access" = "Drupal\erp_transaction\TransactionAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\erp_transaction\TransactionHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "erp_transaction",
 *   data_table = "erp_transaction_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer transaction entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/erp_transaction/{erp_transaction}",
 *     "add-page" = "/admin/structure/erp_transaction/add",
 *     "add-form" = "/admin/structure/erp_transaction/add/{erp_transaction_type}",
 *     "edit-form" = "/admin/structure/erp_transaction/{erp_transaction}/edit",
 *     "delete-form" = "/admin/structure/erp_transaction/{erp_transaction}/delete",
 *     "collection" = "/admin/structure/erp_transaction",
 *     "collection-order" = "/admin/commerce/orders/{commerce_order}/transactions",
 *     "collection-product" = "/product/{commerce_product}/transactions"
 *   },
 *   bundle_entity_type = "erp_transaction_type",
 *   field_ui_base_route = "entity.erp_transaction_type.edit_form"
 * )
 */
class Transaction extends ContentEntityBase implements TransactionInterface
{

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    protected function urlRouteParameters($rel)
    {
        $uri_route_parameters = parent::urlRouteParameters($rel);
        $uri_route_parameters['commerce_order'] = $this->getOrderId();
        return $uri_route_parameters;
    }

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
    {
        parent::preCreate($storage_controller, $values);
        $values += [
        'user_id' => \Drupal::currentUser()->id(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->get('title')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->set('title', $title);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this
            ->get('description')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        $this
        ->set('description', $description);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return $this->get('order_id')->entity;
    }
    /**
     * {@inheritdoc}
     */
    public function getOrderId()
    {
        return $this->get('order_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return $this->get('resource');
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner()
    {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId()
    {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid)
    {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account)
    {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished()
    {
        return (bool) $this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published)
    {
        $this->set('status', $published ? true : false);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Authored by'))
            ->setDescription(t('The user ID of author of the Transaction entity.'))
            ->setRevisionable(true)
            ->setSetting('target_type', 'user')
            ->setSetting('handler', 'default')
            ->setTranslatable(true)
            ->setDisplayOptions(
                'view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
                ]
            )
            ->setDisplayOptions(
                'form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => '60',
                'autocomplete_type' => 'tags',
                'placeholder' => '',
                ],
                ]
            )
            ->setDisplayConfigurable('form', true)
            ->setDisplayConfigurable('view', true);

        $fields['title'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Title'))
            ->setDescription(t('The title of the Transaction entity.'))
            ->setSettings(
                [
                'max_length' => 50,
                'text_processing' => 0,
                ]
            )
            ->setDefaultValue('')
            ->setDisplayOptions(
                'view', [
                'label' => 'above',
                'type' => 'string',
                'weight' => -4,
                ]
            )
            ->setDisplayOptions(
                'form', [
                'type' => 'string_textfield',
                'weight' => -4,
                ]
            )
            ->setDisplayConfigurable('form', true)
            ->setDisplayConfigurable('view', true)
            ->setRequired(false);

        $fields['status'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Publishing status'))
            ->setDescription(t('A boolean indicating whether the Transaction is published.'))
            ->setDefaultValue(true)
            ->setDisplayOptions(
                'form', [
                'type' => 'boolean_checkbox',
                'weight' => -3,
                ]
            );

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Changed'))
            ->setDescription(t('The time that the entity was last edited.'));

        $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Order'))
            ->setDescription(t('The parent order.'))
            ->setSetting('target_type', 'commerce_order')
            ->setDisplayOptions(
                'form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => -1,
                'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => '60',
                'placeholder' => '',
                ],
                ]
            )
            ->setDisplayConfigurable('form', true)
            ->setDisplayConfigurable('view', true) 
            ->setReadOnly(true);
        $fields['resource'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Resource'))
            ->setDescription(t('The resource order item.'))
            ->setSetting('target_type', 'commerce_order_item')
            ->setTargetBundle('erp_default')
            ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
            ->setDisplayOptions(
                'form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => -1,
                'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => '60',
                'placeholder' => '',
                ],
                ]
            )
            ->setDisplayConfigurable('form', true)
            ->setDisplayConfigurable('view', true)
            ->setReadOnly(true);
        $fields['description'] = BaseFieldDefinition::create('text_long')
            ->setLabel(t('Description'))
            ->setDescription(t('A description of the transaction.'))
            ->setTranslatable(true)
            ->setDisplayOptions(
                'view', array(
                'label' => 'hidden',
                'type' => 'text_default',
                'weight' => 0,
                )
            )
            ->setDisplayConfigurable('view', true)
            ->setDisplayOptions(
                'form', array(
                'type' => 'text_textfield',
                'weight' => 0,
                )
            )
            ->setDisplayConfigurable('form', true);    


        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function postSave(EntityStorageInterface $storage, $update = true)
    {
      parent::postSave($storage, $update);


	//kint($storage);

	/* ##STOP per poder debbugar---> Cal posar contingut
 	//Al guardar una transacció hauria de restar la quantitat de recursos de l'STOCK(camp a recurs).
        // When the transaction be saved transaction quantity be subtract of STOCK (@param of resource).
        dpm($storage);
        //Transaccio/Order/obtenir order item?
        $purchasedEntity=$this->storage->getPurchasedEntity();


        if ($purchasedEntity != null) {
            $substract=$purchasedEntity->getQuantity(); //get the order item quantity

            dpm($purchasedEntity);


        }
*/
      if ($this->resource->count() > 0 && $this->order_id->count() > 0 && !$update){
        $messenger = \Drupal::messenger();
        $messenger->addMessage(t('After saving this transaction this is the summary:'), $messenger::TYPE_STATUS);
        //What we need to process : order_item
        //Quan hem de restar? $this->resource->getQuantity();
        foreach($this->resource->referencedEntities() as  $delta => $order_item){
          // The resource to process.
          $resource_wasted = $order_item->getPurchasedEntity();
          // how many?
          $resource_wasted_quantity = $order_item->getQuantity();
          // affect parent order:
          $this->operateInOrderOrderItem($order_item);
          // affect parent order executed quantity
          $this->operateInOrderExecutedQuantity($order_item);
          // affect stock
          $old_qty = $resource_wasted->quantity->value;
          if($order_item->substract->first()->value){
            $resource_wasted->quantity = $resource_wasted->quantity->value - $resource_wasted_quantity;
          }
          else{
            $resource_wasted->quantity = $resource_wasted->quantity->value + $resource_wasted_quantity;
          }
          $messenger->addMessage(t('Resource @resource stock is now at @new_qty units(=@old_qty @substract @relative_qty).',[
            '@resource' => $resource_wasted->label(),
            '@old_qty' => $old_qty,
            '@substract' => ($order_item->substract->first()->value)?"-":"+",
            '@new_qty' => $resource_wasted->quantity->value,
            '@relative_qty' => $resource_wasted_quantity
          ]), $messenger::TYPE_STATUS);

          $resource_wasted->save();

        }

        //Restar processats de la order item de la order

      }
        // Removes quantity of resources from resources list in order.
        /*if ($this->resource->count() > 0 && $this->order->count() > 0){
        foreach($this->resource->referencedEntities() as $delta => $ref_entity){
        $resource_wasted = $ref_entity->resource->resource->referencedEntities->first();
        foreach($this->order->first()->bill_of_materials->referencedEntities() as $delta_order => $ref_resource_bom_order){
          if ($ref_resource_bom_order->resource->target_id == $ref_entity->resource->target_id){
            // Rest to order, order_items  processed
            $ref_resource_bom_order->processed = $ref_entity->resource->resource->referencedEntities->first()->entity->quantity->value;
            // Rest to Product stock
            $ref_entity->resource->first()->entity->quantity = $ref_entity->resource->first()->entity->quantity->value - $ref_entity->resource->resource->referencedEntities->first()->entity->quantity->value;
          }
        }
        }
        }*/
        /* Only change the parents if a value is set, keep the existing values if
        // not.
        if (isset($this->parent->target_id)) {
        $storage
        ->deleteTermHierarchy(array(
        $this
          ->id(),
        ));
        $storage
        ->updateTermHierarchy($this);
        }*/
    }

    /**
     * Operates (Substracts or add) the quantity of resource in parent order
     *
     * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
     *
     * @return bool
     *   If order has this resource and can be added or substracted
     */
    private function operateInOrderOrderItem(OrderItemInterface $order_item){
        $messenger = \Drupal::messenger();
      // For benefit of UX in parent order items processed valeu we will save as 
      // inverted: substract will be add (so it is clear
      // for humans that no negative processed quantity will happen).
      foreach($this->order_id->first()->entity->getItems() as $delta => $parent_order_item){
        if ($parent_order_item->getPurchasedEntity()->id()  == $order_item->getPurchasedEntity()->id()){
          $old_qty = $parent_order_item->processed->value;
          // 
          if($order_item->substract->first()->value){
            $parent_order_item->set('processed', $parent_order_item->processed->value + $order_item->getQuantity());
          }
          else{
            $parent_order_item->set('processed', $parent_order_item->processed->value - $order_item->getQuantity());
          }
          $parent_order_item->save();
           $messenger->addMessage(t('Processed resource @resource in parent order is now at @new_qty units(=@old_qty @substract @relative_qty).',[
            '@resource' => $parent_order_item->label(),
            '@old_qty' => $old_qty,
            '@substract' => ($order_item->substract->first()->value)?"+":"-",
            '@new_qty' => $parent_order_item->processed->value,
            '@relative_qty' => $order_item->getQuantity()
          ]), $messenger::TYPE_STATUS);
          return True;
        }
      }
      return False;
    }

    /**
     * Operates the executed quantity of resource in parent order
     *
     * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
     *
     * @return bool
     *   If order has this target product resource and can be added or substracted
     */
    private function operateInOrderExecutedQuantity(OrderItemInterface $order_item){
      $messenger = \Drupal::messenger();
      // For benefit of UX in parent order items processed valeu we will save as 
      // inverted: substract will be add (so it is clear
      // for humans that no negative processed quantity will happen).
      if ($this->order_id->first()->entity->target_product){
        $target_product = $this->order_id->first()->entity->target_product->first()->entity;
        if ($target_product->id()  == $order_item->getPurchasedEntity()->id()){
          $old_exec_qty = $this->order_id->first()->entity->executed_quantity->value;
          //
          if($order_item->substract->first()->value){
            $this->order_id->first()->entity->set('executed_quantity', $old_exec_qty - $order_item->getQuantity());
          }
          else{
            $this->order_id->first()->entity->set('executed_quantity', $old_exec_qty + $order_item->getQuantity());
          }
          $this->order_id->first()->entity->save();
          $messenger->addMessage(t('Executed quantity of the resource @resource in parent order is now at @new_qty units(=@old_qty @substract @relative_qty).',[
            '@resource' => $target_product->label(),
            '@old_qty' => $old_exec_qty,
            '@substract' => ($order_item->substract->first()->value)?"-":"+",
            '@new_qty' => $this->order_id->first()->entity->executed_quantity->value,
            '@relative_qty' => $order_item->getQuantity()
          ]), $messenger::TYPE_STATUS);
          return True;
        }
      }
      return False;
    }



}


