<?php

namespace Drupal\erp_transaction\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Transaction entities.
 *
 * @ingroup erp_transaction
 */
interface TransactionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface
{

    // Add get/set methods for your configuration properties here.

    /**
     * Gets the Transaction title.
     *
     * @return string
     *   Title of the Transaction.
     */
    public function getTitle();

    /**
     * Sets the Transaction title.
     *
     * @param string $title
     *   The Transaction title.
     *
     * @return \Drupal\erp_transaction\Entity\TransactionInterface
     *   The called Transaction entity.
     */
    public function setTitle($title);

    /**
     * Gets the Transaction creation timestamp.
     *
     * @return int
     *   Creation timestamp of the Transaction.
     */
    public function getCreatedTime();

    /**
     * Sets the Transaction creation timestamp.
     *
     * @param int $timestamp
     *   The Transaction creation timestamp.
     *
     * @return \Drupal\erp_transaction\Entity\TransactionInterface
     *   The called Transaction entity.
     */
    public function setCreatedTime($timestamp);

    /**
     * Returns the Transaction published status indicator.
     *
     * Unpublished Transaction are only visible to restricted users.
     *
     * @return bool
     *   TRUE if the Transaction is published.
     */
    public function isPublished();

    /**
     * Sets the published status of a Transaction.
     *
     * @param bool $published
     *   TRUE to set this Transaction to published, FALSE to set it to unpublished.
     *
     * @return \Drupal\erp_transaction\Entity\TransactionInterface
     *   The called Transaction entity.
     */
    public function setPublished($published);

}
