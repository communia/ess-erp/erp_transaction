<?php

namespace Drupal\erp_transaction\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Transaction type entity.
 *
 * @ConfigEntityType(
 *   id = "erp_transaction_type",
 *   label = @Translation("Transaction type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\erp_transaction\TransactionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\erp_transaction\Form\TransactionTypeForm",
 *       "edit" = "Drupal\erp_transaction\Form\TransactionTypeForm",
 *       "delete" = "Drupal\erp_transaction\Form\TransactionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\erp_transaction\TransactionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "erp_transaction_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "erp_transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/erp_transaction_type/{erp_transaction_type}",
 *     "add-form" = "/admin/structure/erp_transaction_type/add",
 *     "edit-form" = "/admin/structure/erp_transaction_type/{erp_transaction_type}/edit",
 *     "delete-form" = "/admin/structure/erp_transaction_type/{erp_transaction_type}/delete",
 *     "collection" = "/admin/structure/erp_transaction_type"
 *   }
 * )
 */
class TransactionType extends ConfigEntityBundleBase implements TransactionTypeInterface
{

    /**
     * The Transaction type ID.
     *
     * @var string
     */
    protected $id;

    /**
     * The Transaction type label.
     *
     * @var string
     */
    protected $label;

}
