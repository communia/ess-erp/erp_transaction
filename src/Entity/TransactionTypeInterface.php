<?php

namespace Drupal\erp_transaction\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Transaction type entities.
 */
interface TransactionTypeInterface extends ConfigEntityInterface
{

    // Add get/set methods for your configuration properties here.
}
