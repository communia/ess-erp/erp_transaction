<?php

namespace Drupal\erp_transaction;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\erp_transaction\Entity\TransactionInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductInterface;

/**
 * Defines the storage handler class for Transaction entities.
 *
 * This extends the base storage class, adding required special handling for
 * Transaction entities.
 *
 * @ingroup erp_transaction
 */
class TransactionStorage extends SqlContentEntityStorage implements TransactionStorageInterface
{

    /**
     * {@inheritdoc}
    public function revisionIds(TransactionInterface $entity) {
      return $this->database->query(
        'SELECT vid FROM {erp_transaction_revision} WHERE id=:id ORDER BY vid',
        [':id' => $entity->id()]
      )->fetchCol();
    }
     */

    /**
     * {@inheritdoc}
    public function userRevisionIds(AccountInterface $account) {
      return $this->database->query(
        'SELECT vid FROM {erp_transaction_field_revision} WHERE uid = :uid ORDER BY vid',
        [':uid' => $account->id()]
      )->fetchCol();
    }
     */

    /**
     * {@inheritdoc}
    public function countDefaultLanguageRevisions(TransactionInterface $entity) {
      return $this->database->query('SELECT COUNT(*) FROM {erp_transaction_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
        ->fetchField();
    }
     */

    /**
     * {@inheritdoc}
    public function clearRevisionsLanguage(LanguageInterface $language) {
      return $this->database->update('erp_transaction_revision')
        ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
        ->condition('langcode', $language->getId())
        ->execute();
    }
     */

    /**
     * {@inheritdoc}
     */
    public function loadMultipleByOrder(OrderInterface $order)
    {
        $query = $this->getQuery()
            ->condition('order_id', $order->id());
        //->sort('transaction_id');
        $result = $query->execute();

        return $result ? $this->loadMultiple($result) : [];
    }


    /**
     * {@inheritdoc}
     */
    public function loadMultipleByProduct(ProductInterface $product)
    {
        $query = $this->getQuery()
            ->condition('resource.entity.resource.target_id', $product->id());
        //->sort('transaction_id');
        $result = $query->execute();

        return $result ? $this->loadMultiple($result) : [];
    }


}
