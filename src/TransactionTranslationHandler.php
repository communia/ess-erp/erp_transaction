<?php

namespace Drupal\erp_transaction;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for erp_transaction.
 */
class TransactionTranslationHandler extends ContentTranslationHandler
{

    // Override here the needed methods from ContentTranslationHandler.

}
