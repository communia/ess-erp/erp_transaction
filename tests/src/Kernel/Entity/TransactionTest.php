<?php

namespace Drupal\Tests\erp_transaction\Kernel\Entity;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\erp_transaction\Entity\Transaction;
use Drupal\node\Entity\Node;
use Drupal\commerce_price\Price;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Tests\commerce_cart\Traits\CartManagerTestTrait;

/**
 * Tests the transaction entity.
 *
 * @coversDefaultClass \Drupal\erp_transaction\Entity\Transaction
 *
 * @group commerce
 */
class TransactionTest extends OrderKernelTestBase {
  use CartManagerTestTrait;
  /**
   * A sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'node',
    'taxonomy',
    'commerce_tax', //Erp sales order
//    'commerce_cart', // Must be installes via its trait. Used by erp sales order
    'commerce_checkout', //Erp sales order
    'erp_common',
    'erp_order',
    'erp_product_design',
    'erp_contract',
    'erp_transaction',
    'entity_browser',
    'ief_table_view_mode',
    'menu_ui',
    'reference_table_formatter'
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');

    #$this->installConfig(['commerce_product', 'commerce_order','commerce_tax', 'node', 'taxonomy']);
    $this->installConfig(['node', 'taxonomy']);
    $this->installEntitySchema('erp_transaction');
    $this->installCommerceCart();


    $this->installConfig('erp_common');
    $this->installConfig('erp_order');
    $this->installConfig('erp_product_design');
    $this->installConfig('erp_transaction');

    $user = $this->createUser();
    $this->user = $this->reloadEntity($user);

    // First it creates a Basic product, then new product based on composition of basics.
    $basics = $this->createBasicProductVariation();
    $complexes = $this->createCompositionProductVariation();

    // Create a product design of the complex product (variation).
    $product_design = $this->createProductDesign([$basics['variation']],$complexes['variation']->id());

    // Creates a production order vased on product design recipe
    $order_items_in_order = [];
    foreach($product_design->bill_of_materials as $delta => $order_item_from_pd){
      $order_item = OrderItem::create([
        'title' => 'OrderItem in production order #' . $delta,
        'type' => 'erp_production_order',
        'quantity' => (string)($order_item_from_pd->entity->quantity->value * 10), // Product design quantity multiplicated by the order planned_quantity
        'purchased_entity' => $order_item_from_pd->entity->getPurchasedEntity(),
        'unit_price' => new Price('30.00', 'USD')
      ]);
      $order_item->save();
      $order_item = $this->reloadEntity($order_item);
      $order_items_in_order[] = $order_item;
    }
    $order = Order::create([
      'type' => 'erp_production_order',
      'uid' => $this->user->id(),
      'store_id' => $this->store->id(),
      'order_items' => $order_items_in_order,
      'planned_quantity' => 10.0,
      'product_design' => $product_design->id(),
      'target_product' => $complexes['variation']->id()
    ]);
    $order->save();
    $this->order = $this->reloadEntity($order);


    /** Creates a transaction that substracts basic resource cpming from order from stock
     *  And increases the stock of complex resource from order.
    $order_items_in_transaction = [];
    foreach($order->order_items as $delta => $order_item_from_order){
      $order_item_substract = OrderItem::create([
        'title' => 'OrderItem in transaction order #' . $delta,
        'type' => 'erp_default',
        'quantity' => $order_item_from_order->entity->quantity->value,
        'purchased_entity' => $order_item_from_order->getPurchasedEntity()->id(),
        'substract' => TRUE,
        'unit_price' => [
          'number' => '30.00',
          'currency_code' => 'USD',
        ],
      ]);
      $order_item_substract->save();
      $order_item_substract = $this->reloadEntity($order_item_substract);
      $order_items_in_transaction[] = $order_item_substract;
    }
    $order_item_create = OrderItem::create([
        'title' => 'OrderItem in transaction order #' . $delta,
        'type' => 'erp_default',
        'quantity' => 1,
        'purchased_entity' => $order->target_product->entity->id(),
        'substract' => FALSE,
        'unit_price' => [
          'number' => '30.00',
          'currency_code' => 'USD',
        ],
      ]);
    $order_item_create->save();
    $order_item_create = $this->reloadEntity($order_item_create);
    $order_items_in_transaction[] = $order_item_create;

    $transaction = Transaction::create([
      'type' => 'production_note',
      'order' => $order->id(),
      'title' => 'transaction_prnote',
      'resource' => $order_items_in_transaction,
    ]);
    $transaction->save();
    $transaction = $this->reloadEntity($transaction);
     */

    /*
    ASSERTS!!
    assert($complexes['variation']->quantity, 500 - 10 );
    assert($order->executed_quantity->value = 1;
    assert:
      - que stock ha augmentat pel complex.
      - que stock ha baixat pel basic.
      - Que ha augmentat els processats dels order items de la order.
      - Que ha augmentat l'executed_quantity de la order en 1.
      _
     */
  }

  /**
   * Creates a composition of basic products product and its product variation.
   *
   * @return array
   *  with keys "product" and its "product_variation".
   */
  protected function createCompositionProductVariation() {
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $product = Product::create([
      'type' => 'erp_resource',
      'title' => 'Basket of apples',
      'stores' => [$this->store->id()],
    ]);
    $product->save();
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = ProductVariation::create([
      'type' => 'erp_resource',
      'product_id' => $product->id(),
			'minimum_stock_level' => 5,
      'quantity' => 100,
    ]);
    $variation->save();
    $product = $this->reloadEntity($product);
    $variation = $this->reloadEntity($variation);
    return ["product" => $product, "variation" => $variation];

 }

  /**
   * Creates a basic product and its product variation.
   *
   * @return array
   *  with keys "product" and its "product_variation".
   */
  protected function createBasicProductVariation() {
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $product = Product::create([
      'type' => 'erp_resource',
      'title' => 'Apple',
      'stores' => [$this->store->id()],
      'variations' => []
    ]);
    $product->save();
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = ProductVariation::create([
      'type' => 'erp_resource',
      'product_id' => $product->id(),
			'minimum_stock_level' => 5,
      'quantity' => 500,
      'price' => new Price('30.00', 'USD'),
    ]);
    $variation->save();
    $product = $this->reloadEntity($product);
    $variation = $this->reloadEntity($variation);
    return ["product" => $product, "variation" => $variation];

 }

  /**
   * create Product Design.
   *
   * @return NodeInterface
   *   The created product_design.
   */
  protected function createProductDesign($variations, $designed_resource_id) {
    $order_items = [];
    foreach ($variations as $delta => $variation){
      $order_item = OrderItem::create([
        'title' => 'Product design OI#' . $delta,
        'type' => 'erp_production_order',
        'quantity' => 10,
        'purchased_entity' => $variation,
        'unit_price' => new Price('31.00', 'USD'),
      ]);
      $order_item->save();
      $order_item = $this->reloadEntity($order_item);
      $order_items[] = $order_item;
    }

    /** @var \Drupal\core\Entity\NodeInterface $product_design */
    $product_design = Node::create([
      'title' => 'Apple basket design',
      'type' => 'erp_product_design',
      'designed_resource' => $designed_resource_id,
      'bill_of_materials' => $order_items,
    ]);
    $product_design->save();
    $product_design = $this->reloadEntity($product_design);
    return $product_design;
  }

  /**
   * Tests the order integration (total_paid field).
   *
   * @covers ::postSave
   * @covers ::postDelete
   */
  public function testTransactionOperations() {

    $pre_create_stock_quantity = $this->order->target_product->entity->quantity->value;
    $pre_create_processed = [];
    foreach($this->order->order_items as $delta => $order_item_from_order){
      $pre_create_processed[] = $order_item_from_order->entity->processed->value;
      // must be
    }
    $pre_create_executed = $this->order->executed_quantity->value;

    $increase_in_target = 1;
    $waste_in_bom = 10;


    /** Creates a transaction that substracts basic resource coming from order from stock
     *  And increases the stock of complex resource from order.
     */
    $order_items_in_transaction = [];
    foreach($this->order->order_items as $delta => $order_item_from_order){
      $order_item_substract = OrderItem::create([
        'title' => 'OrderItem BOM in transaction order #' . $delta,
        'type' => 'erp_default',
        'quantity' => $waste_in_bom,
        // $order_item_from_order->entity->quantity->value to process  everything
        'purchased_entity' => $order_item_from_order->entity->getPurchasedEntity(),
        'substract' => TRUE,
        'unit_price' => new Price('30.00', 'USD'),
      ]);
      $order_item_substract->save();
      $order_item_substract = $this->reloadEntity($order_item_substract);
      $order_items_in_transaction[] = $order_item_substract->id();
    }
    $order_item_create = OrderItem::create([
        'title' => 'OrderItem target in transaction order #' . $delta,
        'type' => 'erp_default',
        'quantity' => $increase_in_target,
        'purchased_entity' => $this->order->target_product->entity,
        'substract' => FALSE,
        'unit_price' => new Price('30.00', 'USD'),
      ]);
    $order_item_create->save();
    $order_item_create = $this->reloadEntity($order_item_create);
    $order_items_in_transaction[] = $order_item_create->id();

    $transaction = Transaction::create([
      'type' => 'production_note',
      'order_id' => $this->order->id(),
      'title' => 'transaction_prnote',
      'resource' => $order_items_in_transaction,
    ]);
    $transaction->save();
    $transaction = $this->reloadEntity($transaction);
    $order = $this->reloadEntity($this->order);
    $post_create_processed = [];
    foreach($order->order_items as $delta => $order_item_from_order){
      $post_create_processed[] = $order_item_from_order->entity->processed->value;
    }

    $this->assertEquals( $pre_create_executed + $increase_in_target, $order->executed_quantity->value);
    $this->assertEquals([$pre_create_processed[0] + $waste_in_bom], $post_create_processed);
    $this->assertEquals($pre_create_stock_quantity + $increase_in_target, $this->order->target_product->entity->quantity->value);
  }

  /**
   * @covers ::getType
   * @covers ::getPaymentGatewayId
   * @covers ::getPaymentGatewayMode
   * @covers ::getOrder
   * @covers ::getOrderId
   * @covers ::getRemoteId
   * @covers ::setRemoteId
   * @covers ::getRemoteState
   * @covers ::setRemoteState
   * @covers ::getBalance
   * @covers ::getAmount
   * @covers ::setAmount
   * @covers ::getRefundedAmount
   * @covers ::setRefundedAmount
   * @covers ::getState
   * @covers ::setState
   * @covers ::getAuthorizedTime
   * @covers ::setAuthorizedTime
   * @covers ::isExpired
   * @covers ::getExpiresTime
   * @covers ::setExpiresTime
   * @covers ::isCompleted
   * @covers ::getCompletedTime
   * @covers ::setCompletedTime
  public function testPayment() {
    (( @var \Drupal\commerce_payment\Entity\PaymentInterface $payment 
    $payment = Payment::create([
      'type' => 'payment_default',
      'payment_gateway' => 'example',
      'order_id' => $this->order->id(),
      'amount' => new Price('30', 'USD'),
      'refunded_amount' => new Price('10', 'USD'),
      'state' => 'refunded',
    ]);
    $payment->save();

    $this->assertInstanceOf(PaymentDefault::class, $payment->getType());
    $this->assertEquals('example', $payment->getPaymentGatewayId());
    $this->assertEquals('test', $payment->getPaymentGatewayMode());

    $this->assertEquals($this->order, $payment->getOrder());
    $this->assertEquals($this->order->id(), $payment->getOrderId());

    $payment->setRemoteId('123456');
    $this->assertEquals('123456', $payment->getRemoteId());

    $payment->setRemoteState('pending');
    $this->assertEquals('pending', $payment->getRemoteState());

    $this->assertEquals(new Price('30', 'USD'), $payment->getAmount());
    $this->assertEquals(new Price('10', 'USD'), $payment->getRefundedAmount());
    $this->assertEquals(new Price('20', 'USD'), $payment->getBalance());

    $payment->setAmount(new Price('40', 'USD'));
    $this->assertEquals(new Price('40', 'USD'), $payment->getAmount());
    $payment->setRefundedAmount(new Price('15', 'USD'));
    $this->assertEquals(new Price('15', 'USD'), $payment->getRefundedAmount());

    $this->assertEquals('refunded', $payment->getState()->getId());
    $payment->setState('completed');
    $this->assertEquals('completed', $payment->getState()->getId());

    $this->assertEmpty($payment->getAuthorizedTime());
    $payment->setAuthorizedTime(635879600);
    $this->assertEquals(635879600, $payment->getAuthorizedTime());

    $this->assertEmpty($payment->isExpired());
    $payment->setExpiresTime(635879700);
    $this->assertTrue($payment->isExpired());
    $this->assertEquals(635879700, $payment->getExpiresTime());

    $this->assertEmpty($payment->isCompleted());
    $payment->setCompletedTime(635879700);
    $this->assertEquals(635879700, $payment->getCompletedTime());
    $this->assertTrue($payment->isCompleted());
  }
   */

  /**
   * Tests the order integration (total_paid field).
   *
   * @covers ::postSave
   * @covers ::postDelete
  public function testOrderIntegration() {
    $this->assertEquals(new Price('0', 'USD'), $this->order->getTotalPaid());
    $this->assertEquals(new Price('30', 'USD'), $this->order->getBalance());

    (( @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
    $payment = Payment::create([
      'type' => 'payment_default',
      'payment_gateway' => 'example',
      'order_id' => $this->order->id(),
      'amount' => new Price('30', 'USD'),
      'state' => 'completed',
    ]);
    $payment->save();
    $this->order->save();
    $this->assertEquals(new Price('30', 'USD'), $this->order->getTotalPaid());
    $this->assertEquals(new Price('0', 'USD'), $this->order->getBalance());

    $payment->setRefundedAmount(new Price('15', 'USD'));
    $payment->setState('partially_refunded');
    $payment->save();
    $this->order->save();
    $this->assertEquals(new Price('15', 'USD'), $this->order->getTotalPaid());
    $this->assertEquals(new Price('15', 'USD'), $this->order->getBalance());

    $payment->delete();
    // Confirm that if the order isn't explicitly saved, it will be saved
    // at the end of the request.
    $request = $this->container->get('request_stack')->getCurrentRequest();
    $kernel = $this->container->get('kernel');
    $kernel->terminate($request, new Response());
    $this->order = $this->reloadEntity($this->order);
    $this->assertEquals(new Price('0', 'USD'), $this->order->getTotalPaid());
    $this->assertEquals(new Price('30', 'USD'), $this->order->getBalance());
  }

   */
  /**
   * Tests the preSave logic.
   *
   * @covers ::preSave
  public function testPreSave() {
    $request_time = \Drupal::time()->getRequestTime();
    (( @var \Drupal\commerce_payment\Entity\PaymentInterface $payment 
    $payment = Payment::create([
      'type' => 'payment_default',
      'payment_gateway' => 'example',
      'order_id' => $this->order->id(),
      'amount' => new Price('30', 'USD'),
      'state' => 'authorization',
    ]);
    $this->assertEmpty($payment->getPaymentGatewayMode());
    $this->assertEmpty($payment->getRefundedAmount());
    $this->assertEmpty($payment->getAuthorizedTime());
    $this->assertEmpty($payment->getCompletedTime());
    // Confirm that getBalance() works before the payment is saved.
    $this->assertEquals(new Price('30', 'USD'), $payment->getBalance());

    $payment->save();
    $this->assertEquals('test', $payment->getPaymentGatewayMode());
    $this->assertEquals(new Price('0', 'USD'), $payment->getRefundedAmount());
    $this->assertEquals($request_time, $payment->getAuthorizedTime());
    $this->assertEmpty($payment->getCompletedTime());

    $payment->setState('completed');
    $payment->save();
    $this->assertEquals($request_time, $payment->getCompletedTime());
  }
   */

}
